﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_5
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1;
            int num2;
            int num3;
            int negativo;
            int positivo;

            Console.WriteLine("Ingrese el primer numero");
            num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingrese el segundo numero");
            num2 = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingrese el tercer numero");
            num3 = int.Parse(Console.ReadLine());

            if (num1 < 0)
            {
                negativo = num1 * num2 * num3;
                Console.WriteLine("El primer numero es negativo, el producto de los 3 numeros es: " + negativo);
            }
            else
            {
                positivo = num1 + num2 + num3;
                Console.WriteLine("El primer numero es positivo, la suma de los numeros es: " + positivo);
            }
            Console.ReadLine();
        }
    }
}
