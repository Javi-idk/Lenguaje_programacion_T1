﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1;
            int num2;
            int aux;

            Console.WriteLine("Ingresa un numero");
            num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingresa el 2do numero");
            num2 = int.Parse(Console.ReadLine());

            aux = num1;
            num1 = num2;
            num2 = aux;

            Console.WriteLine("El nuevo valor de 1 es: " + num1);
            Console.WriteLine("El nuevo valor de 2 es: " + num2);
            Console.ReadKey();
        }
    }
}
