﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_8
{
    class Program
    {
        static void Main(string[] args)
        {
            Array cadena = null;
            Console.WriteLine("Ingrese una frase:");
            cadena = Console.ReadLine().ToArray();

            for (int i = cadena.Length - 1; i >= 0; i--)
            {
                Console.Write(cadena.GetValue(i));
            }
            Console.ReadLine();
        }
    }
}
