﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1;
            int num2;

            Console.WriteLine("Ingrese un numero");
            num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingrese un numero");
            num2 = int.Parse(Console.ReadLine());

            if (num1 > num2)
            {
                Console.WriteLine("El numero mayor ingresado es " + num1);
            }
            else
            {
                if (num1 == num2)
                {
                    Console.WriteLine("Los numeros ingresados son iguales.");
                }
                else
                {
                    Console.WriteLine("El numero mayor ingresado es " + num2);
                }
            }
            Console.ReadLine();
        }
    }
}
