﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_7
{
    class Program
    {
        static void Main(string[] args)
        {
            String cadena;

            Console.WriteLine("Ingrese una frase");
            cadena = Console.ReadLine();

            int largo = cadena.Length;
            Console.WriteLine("");
            Console.WriteLine("La cadena tiene " + largo + " caracteres");
            Console.WriteLine(cadena.ToUpper());
            Console.ReadLine();
        }
    }
}
