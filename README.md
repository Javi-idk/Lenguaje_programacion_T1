# Tarea Grupal N°1

_Primera tarea entregable en grupo, dentro de este Git realizaremos el desarrollo de 8 ejercicios propuestos en lenguaje C#,
desarrollo del Pseudocodigo y solución del problema en lenguaje natural._

### Ejercicios Propuestos 📋

_Detalle de los ejercicios:_

```
1.- Construir un programa que solicite al usuario 2 variables numéricas (num1 y num2), posterior a esto, intercambie 
los valores de ambas variables y muestre cuánto valen al final las 2 variables.
```
```
2.- Construir un programa que lea 2 números, luego calcule y escriba el valor de la suma, resta, producto y 
división.
```
```
3.- Construir un programa que lea 2 números y nos diga cuál de ellos es el mayor (utilizando el condivional SI).
```
```
4.- Construir un programa que lea 3 números distintos y nos diga cual de ellos es el mayor (recuerda usar la
estructura condicional y operadores lógicos).
```
```
5.- Construir un programa que pida 3 números por teclado, si el primer es negativo, debe imprimir el producto de
los 3 y si no lo es, imprimirá la suma.
```
```
6.- Construir un programa 1ue lea un número, en caso de que ese número sea 0 o menor a 0, terminará el algoritmo
imprimiendo antes un mensaje de error, si es mayor a 0, se deberá calcular se cuadrado, visualizando el numero
que ha teclado el usuario y su resultado.
```
```
7.- Construir un programa que permita ingresar una cadena de caracteres por teclado, lea el ancho de esta cadena
y lo  muestre en pantalla, transforme todas las minúsculas de la cadena en mayúscula.
```
```
8.- Construir un programa que permita ingresar una cadena de caracteres por teclado y la imprima al revés.
```


## Autores ✒️

_El grupo está compuesto por:_

* **Sebastian Santibañez** 
* **Segundo Fuentes** 
* **Cesar Mellafe** 
* **Javiera Soto** 

_Primer trimestre de Ingeniería en Computación e Informática. Lenguajes de programación._

⌨️ con ❤️ por [Javi-idk]

