﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1;
            int num2;
            int num3;

            Console.WriteLine("Ingrese un numero");
            num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingrese el 2do numero");
            num2 = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingrese el 3er numero");
            num3 = int.Parse(Console.ReadLine());


            if (num1 > num2 && num1 > num3)
            {
                Console.WriteLine("El numero mayor es el primer numero ingresado: " + num1);
            }
            else
            {
                if (num2 > num1 && num2 > num3)
                {
                    Console.WriteLine("El numero mayor es el segundo numero ingresado: " + num2);
                }
                else
                {
                    Console.WriteLine("El numero mayor es el tercer numero ingresado: " + num3);
                }
            }
            Console.ReadLine();
        }
    }
}
