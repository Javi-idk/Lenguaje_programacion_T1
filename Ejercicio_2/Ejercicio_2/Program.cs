﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Decimal num1;
            Decimal num2;
            Decimal suma;
            Decimal resta;
            Decimal producto;
            Decimal division;

            Console.WriteLine("Ingrese un numero:");
            num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Ingrese otro numero:");
            num2 = int.Parse(Console.ReadLine());

            suma = num1 + num2;
            resta = num1 - num2;
            producto = num1 * num2;
            division = num1 / num2;

            Console.WriteLine("La suma de los numeros " + num1 + " + " + num2 + " = " + suma);
            Console.WriteLine("La resta de los numeros " + num1 + " - " + num2 + " = " + resta);
            Console.WriteLine("El producto de los numeros " + num1 + " * " + num2 + " = " + producto);
            Console.WriteLine("La division de los numeros " + num1 + " / " + num2 + " = " + division);

            Console.ReadLine();
        }
    }
}
