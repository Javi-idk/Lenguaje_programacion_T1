Inicio programa

Defino variables: num, cuadrado, raiz, raiz2 y cuadradocuadra como REAL
Muestro en pantalla "Ingrese un numero"
Leo numero ingresado por usuario y almaceno en variable num

Si variable 'num' es mayor a 0, entonces:
	Realizo calculo de variable 'num' elevado a variable 'num' y su resultado lo almaceno en variable 'cuadrado'
	Realizo calculo de variable 'num' elevado al cuadrado y su resultado lo almaceno en variable 'cuadradocuadra'
	Realizo calculo de raiz cuadrada de variable 'num' y su resultado lo almaceno en variable 'raiz'
	Realizo calculo de raiz cuadrada de variable 'cuadradocuadra'
	Muestro en pantalla: "El número ingresado es " + variable 'num' + " elevado por si mismo es " + variable 'cuadrado' + " y su raiz es " + raiz + " El numero ingresado, elevado a su cuadrado ^2 es " + variable 'cuadradocuadra' + " y su raiz es " + variable 'raiz2'

Si variable 'num' es menor o igual a 0, entonces muestro en pantalla: "Error"

Fin programa
