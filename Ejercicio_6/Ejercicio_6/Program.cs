﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_6
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            double cuadrado;
            double raiz;
            double raiz2;
            double cuadradocuadra;

            Console.WriteLine("Ingrese un numero");
            num = int.Parse(Console.ReadLine());

            if (num > 0)
            {
                cuadrado = Math.Pow(num, num);
                cuadradocuadra = Math.Pow(num, 2);
                raiz = Math.Sqrt(num);
                raiz2 = Math.Sqrt(cuadradocuadra);
                Console.WriteLine("El numero ingresado es " + num + " elevado por si mismo es " + cuadrado + " y su raiz es " + raiz +
                    ". El numero ingresado, elevado a su cuadrado ^2 es " + cuadradocuadra + " y su raiz es " + raiz2);
            }
            else
            {
                Console.WriteLine("Error");
            }
            Console.ReadLine();
        }
    }
}
